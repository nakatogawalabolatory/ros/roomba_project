#!/usr/bin/env python3
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription, LaunchContext
from launch.actions import IncludeLaunchDescription, LogInfo
from launch_xml.launch_description_sources import XMLLaunchDescriptionSource
from launch_ros.actions import Node

import traceback
import rclpy
import os

def generate_launch_description():
    # launch description
    ld = LaunchDescription()
    # launch pkg name
    create_bringup = get_package_share_directory("create_bringup")
    try:
        # msg test
        ld.add_action(LogInfo(
                        #condition=launch.conditions.IfCondition(True),  # Always execute the loginfo action
                        msg="""
@@@@@@@@@@@@@@@@@@@@@@@@@@@
@ WELCOME                 @
@ ROOMBA MINIMUM LAUNCH   @
@@@@@@@@@@@@@@@@@@@@@@@@@@@
                        """
        ))
        # device check
        if not os.path.exists("/dev/ttyUSB0"):
            ld.add_action(LogInfo(
                        #condition=launch.conditions.IfCondition(True),  # Always execute the loginfo action
                        msg="""
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@ ERROR!!                      @
@ ROOMBA IS NOT FOUND          @
@ CONNECT THE SERIAL CABLE     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                        """
            ))

        # voicevox
        voicevox = Node(
            package = "voicevox_ros2",
            executable = "voicevox_ros2_core",
        )
        ld.add_action(voicevox)

        # create bringup
        create_robot = IncludeLaunchDescription(
            XMLLaunchDescriptionSource([create_bringup + "/launch/create_2.launch"]),
            #launch_arguments={
            #    'output' : 'screen'
            #}
        )
        ld.add_action(create_robot)

        # Joy node and joy_teleop
        joy_node = Node(
            package='joy',
            executable = 'joy_node'
        )
        joy_teleop = Node(
            package='roomba_task',
            executable = 'joy_teleop'
        )
        ld.add_action(joy_node)
        ld.add_action(joy_teleop)

        # ステータスチェッカー
        roomba_status_check = Node(
            package = "roomba_task",
            executable = "battery_check",
        )
        ld.add_action(roomba_status_check)

        # Boot up
        boot_up = Node(
            package='roomba_task',
            executable = 'boot_up'
        )
        ld.add_action(boot_up)

        return ld
    except:
        traceback.print_exc()
