import os
from glob import glob
from setuptools import setup

package_name = 'roomba_project'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join("share", package_name), glob("launch/*.py")),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='roboworks',
    maintainer_email='nakatogawagai@gmail.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            "teleopkey = roomba_project.keyboad_control:main",
            "boot_up = roomba_project.roomba_project_common.music:bootup",
            "orientation = roomba_project.common.move_base:main",
            "battery_check = roomba_project.common.check_battery:main",
            "joy_teleop = roomba_project.common.joy_teleop:main"
        ],
    },
)
