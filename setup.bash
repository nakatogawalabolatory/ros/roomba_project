current_dir=$(dirname "$(readlink -f "$0")")

cd $current_dir

pypath=$current_dir/roomba_project

# bashrc
# チェックする文字列
search_string="#roomba_project setting"
# チェックするファイルパス
bashrc_path=~/.bashrc

if grep -qF "$search_string" "$bashrc_path"; then
    :
else
    echo "#roomba_project setting" >> ~/.bashrc
    echo "export PYTHONPATH=\"\$PYTHONPATH:$pypath\"" >> ~/.bashrc
fi
