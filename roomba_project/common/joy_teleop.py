#!/usr/bin/env python3
from  sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist
from create_msgs.msg import MotorSetpoint
import rclpy
import threading
import traceback

twist = Twist()
vacuum = MotorSetpoint()
main_brush = MotorSetpoint()
side_brush = MotorSetpoint()
twist_pub = main_brush_pub = vac_pub = None
vacuum_flag = False

def cb_joy(msg):
    global twist_pub, vac_pub, main_brush_pub, side_brush_pub
    global vacuum_flag
    
    if msg.buttons[5]:
        # twist
        twist.linear.x = msg.axes[1]* 0.25 * (msg.buttons[7] + 1)
        twist.angular.z = msg.axes[0]*2.175 * (msg.buttons[7] + 1)

        # vacuum and brush
        main_brush.duty_cycle = side_brush.duty_cycle = vacuum.duty_cycle = 1.0 if msg.buttons[8]==1 else 0.0

        twist_pub.publish(twist)
        vac_pub.publish(vacuum)
        main_brush_pub.publish(main_brush)
        side_brush_pub.publish(side_brush)

def main():
    global twist_pub, vac_pub, main_brush_pub, side_brush_pub

    rclpy.init()
    node = rclpy.create_node('joy_teleop')
    node.get_logger().info("ROOMBA JOY TELEOP START")

    joy_sub = node.create_subscription(Joy, 'joy',cb_joy, 10)
    twist_pub = node.create_publisher(Twist, 'cmd_vel', 10)
    vac_pub = node.create_publisher(MotorSetpoint, 'vacuum_motor', 10)
    main_brush_pub = node.create_publisher(MotorSetpoint, 'main_brush_motor', 10)
    side_brush_pub = node.create_publisher(MotorSetpoint, 'side_brush_motor', 10)

    rclpy.spin(node)

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
       pass
