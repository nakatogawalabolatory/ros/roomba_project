#!/usr/bin/env python3
import rclpy
import threading
import os
import time
from rclpy.node import Node

from voicevox_ros2_interface.msg import Speaker
from create_msgs.msg import Cliff
from std_msgs.msg import Float32, Empty

# ルンバステータス変数
cc_battery_cap = cc_battery_char = cc_battery_cur = cc_cliff = cc_wd = None

# バッテリーキャパ
def _cb_battery_cap(msg):
    global cc_battery_cap
    cc_battery_cap = msg.data

# バッテリーチャージ
def _cb_battery_char(msg):
    global cc_battery_char
    cc_battery_char = msg.data

# バッテリーカレント
def _cb_battery_cur(msg):
    global cc_battery_cur
    cc_battery_cur = msg.data
    if cc_battery_cur < -0.15:
        cc_battery_cur = False
    else:
        cc_battery_cur = True

# 段差センサー
def _cb_cliff(msg):
    global cc_cliff
    cc_cliff = [
            msg.is_cliff_left,
            msg.is_cliff_front_left,
            msg.is_cliff_front_right,
            msg.is_cliff_right
    ]

# ホイールドロップ


def main():
    rclpy.init()
    node = rclpy.create_node("roomba_check_battery")
    print(str(type(node)))

    # スリープ周期を設定
    rate = node.create_rate(0.5)
    # voicevox の起動を確認する変数
    vv_topic_info = 0
    # voicevox の未起動時間を計測するための変数
    init_vv_time = time.time()
    # ルンバから信号が来ているかどうかを確認する変数
    init_cc_time = time.time()
    cc_topic_info = 0
    cc_state = True
    # ルンバのバッテリー残量の初期値
    battery_level = 0
    # ルンバの車輪状態初期値
    cc_wd = False
    # 起動報告
    node.get_logger().info("ROOMBA CHECK BATTERY STATE START")

    # メッセージを格納する変数
    text = ''
    # メッセージを伝えたら立ち上がるフラグ
    msg_frag = False
    # メッセージフラグのバックアップ
    msg_frag_bu = None
    # 各ステータスの状態をバックアップするリスト
    sbu_list = [
            None, # ttyUSB
            None, # roomba signal
            None, # roomba battery
            None, # roomba charging
            None, # roomba cliff
            None, # roomba wheeldrop
    ]

    # スピーカーのパブリッシャー
    sp = Speaker()
    pub_msg = node.create_publisher(Speaker, "/voicevox_ros2/speaker", 10)

    ## ルンバのステータスサブスクライバー
    # バッテリー関連
    cc_cap_sub = node.create_subscription(Float32, "/battery/capacity", _cb_battery_cap, 10)
    cc_char_sub = node.create_subscription(Float32, "/battery/charge", _cb_battery_char, 10)
    # 充電関連
    cc_cur_sub = node.create_subscription(Float32, "/battery/current", _cb_battery_cur, 10)
    # 段差センサー
    cc_cliff_sub = node.create_subscription(Cliff, "/cliff", _cb_cliff, 10)
    # ホイールドロップ
    def _cb_wd(msg):
        if all(r is None for r in sbu_list):
            pass
        cc_wd = True
        print("cc")
        #text = "ホイールドロップ！"
    cc_wd_sub = node.create_subscription(Empty, "/wheeldrop", _cb_wd, 10)

    # Spin in a separate thread
    thread = threading.Thread(target=rclpy.spin, args=(node, ), daemon=True)
    thread.start()

    rate = node.create_rate(2)

    try:
        while rclpy.ok():
            # Voicevox サブスクライバーの存在を確認
            try:
                # トピック情報を取得
                if vv_topic_info == 0:
                    node.get_logger().debug("VOICEVOX: VOICEVOX WAIT BROADCAST")
                    rate.sleep()
                vv_topic_info = len(node.get_subscriber_names_and_types_by_node(node_name="voicevox_ros2_core", node_namespace=""))
            except rclpy._rclpy_pybind11.NodeNameNonExistentError:
                if time.time() - init_vv_time > 5:
                    node.get_logger().warn("VOICEVOX: VOICEVOX SPEAKER IS NOT RUNNING...")
                vv_topic_info = 0
                continue
            # ttyUSB の状態を確認
            tty_state = os.path.exists("/dev/ttyUSB0")
            if tty_state is True and tty_state != sbu_list[0] and sbu_list[0] != True and sbu_list[0] != None:
                node.get_logger().fatal("CONNECTION SUCCESS: SERIAL CABLE IS CONNECTED. BUT YOU HAHE TO RESTART PROCESS")
                node.get_logger().fatal("PLEASE SHUTDOWN THIS PROCESS ! ROOMBA IS DEAD !")
                text = "シリアルケーブルの接続を確認しましたが、再起動が必要です。"
            if tty_state is True and tty_state != sbu_list[0] and sbu_list[0] != False:
                #if sbu_list[1] == True:
                node.get_logger().info("CONNECTION SUCCESS: SERIAL CABLE IS CONNECTED")
                text = "ルンバ、起動しました。"
            elif tty_state is False and sbu_list[0] == None:
                node.get_logger().error("CONNECTION ERROR: SERIAL CABLE IS NOT CONNERCTED")
                text = "エラー！シリアルケーブルが接続されていません。"
            elif tty_state is False and tty_state != sbu_list[0] and sbu_list[0] != None:
                node.get_logger().error("CONNECTION ERROR: SERIAL CABLE IS DISCONNECTED")
                text = "重篤なエラー！シリアルケーブルが切断されました。"

            # ルンバからの信号を確認
            if tty_state == True:
                try:
                    cc_topic_info = len(node.get_subscriber_names_and_types_by_node(node_name="create_driver", node_namespace=""))
                    #node.get_logger().info(str(cc_topic_info))
                    if cc_topic_info > 1:
                        cc_state = True
                        init_cc_time = time.time()
                    else:
                        #node.get_logger().warn("CONNECTION WARN:")
                        if time.time() - init_cc_time > 1:
                            cc_state = False
                        else:
                            cc_state = True
                except rclpy._rclpy_pybind11.NodeNameNonExistentError:
                    cc_state = False
                if cc_state is True and cc_state != sbu_list[1] and sbu_list[1] != True and sbu_list[1] != None:
                    node.get_logger().info("CONNECTION SUCCESS: SIGNAL REVIVAL")
                    text = text + "通信が再確立しました。" 
                if cc_state is True and cc_state != sbu_list[1] and sbu_list[1] != False:
                    pass
                elif cc_state is False and sbu_list[1] == None:
                    node.get_logger().warn("CONNECTION WARN: PLEASE PUSH THE CLEAN BUTTON")
                    text = text + "しかしトピックを取得できませんでした。クリーンボタンを押してください。"
                elif cc_state is False and cc_state != sbu_list[1] and sbu_list[1] != None:
                    node.get_logger().warn("CONNECTION WARN: SIGNAL LOST")
                    text = text + "エラー！ルンバからの通信が切断されました。クリーンボタンを押してください。"

            # ルンバの充電状態を取得
            if cc_battery_cur != None and cc_battery_cur != sbu_list[3] and sbu_list[3] != None:
                if cc_battery_cur:
                    text = text + "充電中。"
                else:
                    text = text + "ドックから離れました。"
            # ルンバのバッテリー残量を取得
            if cc_battery_cap != None and cc_battery_char != None and not cc_battery_cur:
                battery_level = int((cc_battery_char/cc_battery_cap) * 100)
                #print(battery_level)
                if battery_level != sbu_list[2]:
                    if battery_level % 5 == 0 and battery_level > 20 and sbu_list[2] != None:
                        text = text + "残りバッテリー残量%dパーセント"%(battery_level)
                    elif battery_level <= 20:
                        node.get_logger().warn("BATTERY %d: LOW BATTERY"%(battery_level))
                        text = text + "ローバッテリー警告。残りバッテリー残量%dパーセント。"%(battery_level)
                    elif battery_level <= 10:
                        node.get_logger().warn("BATTERY %d: CRITICAL LOW BATTERY"%(battery_level))
                        text = text + "充電してください。残りバッテリー残量%dパーセント。"%(battery_level)

            # ルンバの段差センサーからの状態を取得
            if cc_cliff != None and cc_cliff != sbu_list[4]:
                if sbu_list[4] == None and any(cc_cliff):
                    node.get_logger().warn("ROOMBA : BAD ENVIROMENT. CLIFF IS EXIST")
                    text = text + "起動環境に問題があります。段差を検出しました。"
                elif any(cc_cliff):
                    node.get_logger().warn("ROOMBA : CLIFF")                

            # ルンバの車輪脱落状態を取得
            """
            if cc_wd != sbu_list[5]:
                print("wd!!")
            if cc_wd is True:
                print("changed")
                cc_wd = False
            """
            # ステータスを報告
            if len(text) > 0:
                sp.text = text
                sp.id = 2
                # メッセージをパブリッシュ
                pub_msg.publish(sp)
                text = ""
            # バックアップを作成
            sbu_list = [
                    tty_state,
                    cc_state,
                    battery_level,
                    cc_battery_cur,
                    cc_cliff,
                    cc_wd,
            ]

    except KeyboardInterrupt:
        pass

    rclpy.shutdown()
    thread.join()

if __name__ == "__main__":
    main()
