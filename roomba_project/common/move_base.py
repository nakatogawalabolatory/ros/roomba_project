#!/usr/bin/env python3
import rclpy
from rclpy.node import Node

from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry

from tf_transformations import euler_from_quaternion, quaternion_from_euler

X = Y = ROLL = PITCH = YAW = None
BREAKER = False

class Orientation(Node):
    def __init__(self):
        """
        get target angle
        compare direction in current position
        """
        super().__init__("roomba_move_base")

        self.X = self.Y = self.PITCH = self.ROLL = self.YAW = None
        self.Q = self.target_q = []
        self.twist = Twist()

        self.target_angle = 90
        #self.target_q = quaternion_from_euler(0,0,target_angle)
        self.get_logger().info("Target angle : %f" % self.target_angle)

        self.move_base = self.create_publisher(Twist, "cmd_vel", 10)
        self.get_odom = self.create_subscription(Odometry, "odom", self.odom_cd, 10)

    def odom_cd(self,msg):
        global BREAKER
        self.X = msg.pose.pose.position.x
        self.Y = msg.pose.pose.position.y
        self.Q = (msg.pose.pose.orientation.x,
                    msg.pose.pose.orientation.y,
                    msg.pose.pose.orientation.z,
                    msg.pose.pose.orientation.w)

        EULER = euler_from_quaternion(self.Q)
        #print(EULER[2])
        #print(self.X, self.Y)
        orient = (self.target_angle - EULER[2]) * 1.5
        if orient < 0.05 and orient > -0.05:
            BREAKER = True
        else:
            self.twist.angular.z = orient
            self.move_base.publish(self.twist)

#class rlt_Linear(Node):

def main():
        rclpy.init()
        node = Orientation()
        while rclpy.ok() and BREAKER is False:
            try:
                rclpy.spin_once(node)
            except KeyboardInterrupt:
                break
        node.destroy_node()
        print("shutdown")
        rclpy.shutdown()

if __name__ == "__main__":
    main()
