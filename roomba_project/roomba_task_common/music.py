#!/usr/bin/env python3
from rclpy.node import Node
import rclpy
import time

from create_msgs.msg import DefineSong, PlaySong

class PerformSong():
    def __init__(self,node, play="bootup"):
        self.node = node
        self.node.get_logger().info(
            f"""
SELECT THE SONGS
"""
        )

        self.set_song = self.node.create_publisher(DefineSong, "/define_song", 10)
        self.perform_song = self.node.create_publisher(PlaySong, "/play_song", 10)

        #if play == "bootup":
        #    self.bootup()
        #elif play == "highwindallback":
        #    self.HighWindAllBack()
        #elif play == "dna":
        #    self.DanceTheNightAway()

    def bootup(self):
        ## music setting
        BPM = 240
        crotchet = 60/BPM # 四分音符
        minim = crotchet * 2 # 二分音符 
        whole = crotchet * 4 # 全音符
        eighth = crotchet / 2  # 八分音符
        ##
        ds = DefineSong()
        song = 0
        notes = [
            67,0,67,72,74,79
        ]
        durations = [
            eighth,
            eighth,
            eighth,
            crotchet * 1.5,
            crotchet,
            minim
        ]
        self.song_diag(ds)

    def ErrorSound0(self):
        BPM=120
        c=60/BPM

        ds0 = DefineSong()

        ds0.song = 0
        ds0.notes = [67, 62, 70, 69]
        ds0.durations = [c, c/2, c, c]
        ds0.length = len(ds0.notes)

        self.song_diag(ds0)

    def HighWindAllBack(self):
        ## music setting
        BPM = 135 # 135
        c = 60/BPM # 四分音符

        ds0 = DefineSong()
        ds1 = DefineSong()

        ds0.song = 0
        ds0.notes = [
            71, # so
            0,
            64, # to
            0,
            64, # de
            62, # da
            64, # shu
            67, # n
            64, # kan
            0,
            ] 
        ds0.durations = [
            c/4, #so
            c/2,
            c/4, #to
            c/8,
            c/2, #de
            c/2, #ta
            c/2, #shu
            c/2, #n
            c/2, #kan
            c/2
        ]
        ds0.length = len(ds0.notes)
        #            o wa tta wa
        ds1.song = 0    
        ds1.notes = [64,67,71,69]
        ds1.durations = [c/2,c,c/2,c]
        ds1.length = len(ds1.notes)        

        self.song_diag(ds0)
        self.song_diag(ds1)

    def Idol(self):
        BPM = 166 
        c = 60/BPM # 四分音符

        oct = -12

        ds0 = DefineSong()
        ds1 = DefineSong()

        ds0.song = 0
        ds0.notes = [76+oct, 79+oct, 81+oct, 88+oct, 81+oct, 79+oct, 81+oct, 88+oct, 81+oct, 79+oct, 81+oct]
        ds0.durations = [c/2, c/2, c, c, c, c/2, c, c/2, c/2, c/2, c]
        ds0.length = len(ds0.notes)

        ds1.song = 0
        ds1.notes = [64,67,69, 72, 71, 67, 69 , 71, 72, 74, 77, 76]
        ds1.durations = [c/2, c/2, c, c, c, c/2, c, c/2, c/2, c/2, c, c]
        ds1.length = len(ds1.notes)
        
        self.song_diag(ds0)
        self.song_diag(ds1)

    def song_diag(self, ds):
        if len(ds.notes) <= 16:
            if len(ds.notes) != len(ds.durations):
                self.node.get_logger().info(
                "Warning: notes(%d) and durations(%d) is not same index" %(len(ds.notes), len(ds.durations))) 

            self._publish(ds)
        else:
            self.node.get_logger().info("ERROR: index is put of range or not match notes and durations")

    def _publish(self, define_song):
        ps = PlaySong()
        ps.song = define_song.song
        self.set_song.publish(define_song)

        self.node.get_logger().info("Publish: No. %d"%(define_song.song))
        self.perform_song.publish(ps)
        time.sleep(sum(define_song.durations))

def bootup():
    try:
        rclpy.init()
        node = rclpy.create_node('roomba_bootup')
        time.sleep(3)
        p = PerformSong(node)
        #p.HighWindAllBack()
        #p.ErrorSound0()
        p.Idol()
    except KeyboardInterrupt:
        print('done')
        pass

if __name__ == "__main__":
    main()
