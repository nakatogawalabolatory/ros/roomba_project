#!/usr/bin/env python3
from rclpy.node import Node
from std_msgs.msg import Empty
import rclpy
import smach
import traceback
import time

class WaitPushCleanButton(smach.State):
    def __init__(self,node,
                 success_msg='Pushed clean button. task will start',
                 timeout_msg='timeout. clean button was not push.',
                 timeout=120):
        smach.State.__init__(self,
                          outcomes=['success','timeout','failure'])
        self.node = node
        self.timeout = timeout
        self.timeout_msg = timeout_msg
        self.success_msg = success_msg

        self.timeout_flag = False
        self.return_msg = 'timeout'

    def _cb_cleanbutton(self, msg):
        self.return_msg = 'success'
        self.timeout_flag = True
        self.node.get_logger().info(self.success_msg)

    def _cb_timeout(self):
        self.timeout_flag = True
        self.node.get_logger().warn(self.timeout_msg)

    def execute(self, userdata):
        try:
            # get_callback
            push_cb = self.node.create_subscription(Empty,'/clean_button',self._cb_cleanbutton, 10)
            # timeout
            timeout_timer = self.node.create_timer(self.timeout, self._cb_timeout)
            while not self.timeout_flag:
                rclpy.spin_once(self.node)
            # stop subscription and timeout
            timeout_timer.cancel()
            push_cb.destroy()
            return self.return_msg

        except:
            self.node.get_logger().error(traceback.format_exc())
            return 'failure'
