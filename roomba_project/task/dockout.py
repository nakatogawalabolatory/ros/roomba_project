#!/usr/bin/env python3
### rclpy pkgs
import rclpy
from rclpy.node import Node
### smach
import smach
### msgs
# general
from std_msgs.msg import Empty, String
# roomba
from roomba_task_common.wait_push_button import WaitPushCleanButton
# voicevox
from voicevox_ros2_interface.msg import Speaker
from voicevox_ros2_tts import tts_SpeakerState
# pther pkg
import time

### ノードの作成
rclpy.init()
node = Node('dockout_state')

### ステートマシーンの作成
def create_sm():
    """
    success : タスク成功
    failure : タスク失敗
    timeout : タイムアウト
    """
    sm = smach.StateMachine(
                    outcomes=['success', 'failure', 'timeout'])

    # task name
    sm.userdata.task_name = 'dockout'
    # timeout (sec)
    sm.userdata.timeout = 120

    ### create state machine
    with sm:
        ### Task declaration
        smach.StateMachine.add('TASK_DECLARATION', tts_SpeakerState(node=node,
                                                                    text='クリーンボタンを押してください',
                                                                    timeout=3),
                                transitions={'success':'PUSH_CLEAN_BUTTON',
                                             'timeout':'timeout',
                                             'failure':'failure'})
        ### robot state check
        ### push the clean button
        smach.StateMachine.add('PUSH_CLEAN_BUTTON', WaitPushCleanButton(node=node,
                                                                       timeout=5),
                                transitions={'success':'success',
                                             'timeout':'timeout',
                                             'failure':'failure'})

        ### dockout
    return sm

### メイン
def main():
    ### ステートマシーンの作成
    sm = create_sm()
    outcomes = sm.execute() # ステートマシーンからの最終遷移状態を取得

    ### ステートマシーンからの応答を出力
    if outcomes == 'success':
        node.get_logger().info('Done!')
    elif outcomes == 'timeout':
        node.get_logger().warn('timeout.')
    else:
        node.get_logger().error('Some error happened')
    
    ### shutdown
    node.destroy_node()
    rclpy.shutdown()

### execute
if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
