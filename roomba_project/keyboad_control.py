#!/usr/bin/env python3
from pynput.keyboard import Controller, Key, Listener

import rclpy
from rclpy.node import Node

from geometry_msgs.msg import Twist

class KeyboadTeleop(Node):
    def __init__(self):
        super().__init__("roomba_keyboadteleop")
        self.pub = self.create_publisher(Twist, "/cmd_vel", 10)

        self.x = self.z = 0
        self.LINEAR_SPEED = 0.5
        self.ANGULAR_SPEED = 1.0
        self.key_bindings = {
            "w": (self.LINEAR_SPEED, 0.0),
            "s": (-self.LINEAR_SPEED, 0.0),
            "a": (0.0, self.ANGULAR_SPEED),
            "d": (0.0, -self.ANGULAR_SPEED),
        }

        self.key_listener = Listener(
            on_press = self.twist_on,
            on_release = self.twist_off,
        )
        self.key_listener.start()
        self.get_logger().info(
            f"""
ROOMBA KEYBOAD TELEOP

BASIC CONTROL:
Control the twist. W, S is linear. A, D is angular.
Default speed is linear : 0.15, angular : 1.0
    W
A   S   D

OTHER CONTROL
R :
F :
0 ~ 9 :

QUIT:
Press CTRL + C
"""
        )

    def twist_on(self, key):
        bind = None
        if self._is_special_key(key) is False:
            if key.char in self.key_bindings:
                bind = self.key_bindings[key.char]
                self.x = bind[0]
                self.z = bind[1]
                self.twist_pub()
        else:
            pass

    def twist_off(self, key):
        self.x = 0
        self.z = 0
        self.twist_pub()

    def _is_special_key(self, key):
        try:
           key.char
           return False
        except AttributeError:
           return True

    def twist_pub(self):
        tw = Twist()
        tw.linear.x = float(self.x)
        tw.angular.z = float(self.z)
        self.pub.publish(tw)

def main():
    try:
        rclpy.init()
        node = KeyboadTeleop()
        rclpy.spin(node)
        node.destroy_node()
        rclpy.shutdown()
    except KeyboardInterrupt:
        print("\n")
        pass

if __name__ == "__main__":
    main()
